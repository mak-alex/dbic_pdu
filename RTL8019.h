/*****************************************************************************
����: RTL8019.h
Created by PROTTOSS Mail to PROTTOSS@mail.ru

RTL8019AS driver

05.06.2007
******************************************************************************/

#ifndef RTL_8019AS_DRIVER_H
#define RTL_8019AS_DRIVER_H
/*****************************************************************************
******************************************************************************/

#include "..\netbuf.h"
#include "..\integer.h"



/*****************************************************************************
******************************************************************************/

/* Command register */
#define CR 			0x00
#define IOPORT 		0x10    /* I/O data port */
#define RESET 		0x1f     /* Reset port */

/* Page 0 register offsets.*/
#define CLDA0 		0x01    /* Current local DMA address 0 */
#define PSTART 		0x01	/* Page start register */
#define CLDA1 		0x02    /* Current local DMA address 1 */
#define PSTOP 		0x02    /* Page stop register */
#define BNRY 		0x03	/* Boundary pointer */
#define TSR 		0x04	/* Transmit status register */
#define TPSR 		0x04	/* Transmit page start address */
#define NCR 		0x05	/* Number of collisions register */
#define TBCR0 		0x05    /* Transmit byte count register 0 */
#define FIFO 		0x06	/* FIFO */
#define TBCR1 		0x06    /* Transmit byte count register 1 */
#define ISRRTL 		0x07	/* Interrupt status register */
#define CRDA0 		0x08    /* Current remote DMA address 0 */
#define RSAR0 		0x08    /* Remote start address register 0 Low byte address to read from the buffer. */
#define CRDA1 		0x09    /* Current remote DMA address 1 */
#define RSAR1 		0x09    /* Remote start address register 1 High byte address to read from the buffer. */
#define RBCR0 		0x0a    /* Remote byte count register 0 Low byte of the number of bytes to read from the buffer. */
#define RBCR1 		0x0b    /* Remote byte count register 1 High byte of the number of bytes to read from the buffer. */
#define RSR 		0x0c	/* Receive status register */
#define RCR 		0x0c	/* Receive configuration register */
#define CNTR0 		0x0d    /* Tally counter 0 (frame alignment errors) */
#define TCR 		0x0d	/* Transmit configuration register */
#define CNTR1 		0x0e    /* Tally counter 1 (CRC errors) */
#define DCR 		0x0e	/* Data configuration register */
#define CNTR2 		0x0f    /* Tally counter 2 (Missed packet errors) */
#define IMR 		0x0f	/* Interrupt mask register */

/* Page 1 register offsets. */
#define PAR0 		0x01     /* Physical address register 0 */
#define PAR1 		0x02     /* Physical address register 1 */
#define PAR2 		0x03     /* Physical address register 2 */
#define PAR3 		0x04     /* Physical address register 3 */
#define PAR4 		0x05     /* Physical address register 4 */
#define PAR5 		0x06     /* Physical address register 5 */
#define CURR 		0x07     /* Current page register. The next incoming packet will be stored at this page address. */
#define MAR0 		0x08     /* Multicast address register 0 */
#define MAR1 		0x09     /* Multicast address register 1 */
#define MAR2 		0x0a     /* Multicast address register 2 */
#define MAR3 		0x0b     /* Multicast address register 3 */
#define MAR4 		0x0c     /* Multicast address register 4 */
#define MAR5 		0x0d     /* Multicast address register 5 */
#define MAR6 		0x0e     /* Multicast address register 6 */
#define MAR7 		0x0f     /* Multicast address register 7 */

/*
 * Page 2 register offsets.
 */
#define PSTART 		0x01   /* Page start register */
#define CLDA0 		0x01    /* Current local DMA address 0 */
#define PSTOP 		0x02    /* Page stop register */
#define CLDA1 		0x02    /* Current local DMA address 1 */
#define RNP 		0x03      /* Remote next packet pointer */
#define TSPR 		0x04     /* Transmit page start register */
#define LNP 		0x05      /* Local next packet pointer */
#define ACU 		0x06      /* Address counter (upper) */
#define ACL 		0x07      /* Address counter (lower) */
#define RCR 		0x0c      /* Receive configuration register */
#define TCR 		0x0d      /* Transmit configuration register */
#define DCR 		0x0e      /* Data configuration register */
#define IMR 		0x0f      /* Interrupt mask register */

/*
 * Page 3 register offsets.
 */
#define EEPROMCR   	0x01    /* EEPROM command register */
#define BPAGE    	0x02    /* Boot-ROM page register */
#define CONFIG0  	0x03    /* Configuration register 0 (r/o) */
#define CONFIG1  	0x04    /* Configuration register 1 */
#define CONFIG2  	0x05    /* Configuration register 2 */
#define CONFIG3  	0x06    /* Configuration register 3 */
#define CSNSAV   	0x08    /* CSN save register (r/o) */
#define HLTCLK   	0x09    /* Halt clock */
#define INTR     	0x0b    /* Interrupt pins (r/o) */
#define CONFIG4  	0x0d    /* Configuration register 3 */

/* Command register bits. */
#define CR_STP 			0x01    /* Stop */
#define CR_STA 			0x02    /* Start */
#define CR_TXP 			0x04    /* Transmit packet */
#define CR_RD0 			0x08    /* Remote DMA command bit 0 */
#define CR_RD1 			0x10    /* Remote DMA command bit 1 */
#define CR_RD2 			0x20    /* Remote DMA command bit 2 */
#define CR_PS0 			0x40    /* Page select bit 0 */
#define CR_PS1 			0x80    /* Page select bit 1 */

/* Command register page bits */
#define REG_PAGE0		0x00
#define REG_PAGE1		CR_PS0
#define REG_PAGE2		CR_PS1
#define REG_PAGE3		(CR_PS1 | CR_PS0)

/* Command register DMA bits */
#define DMA_READ		CR_RD0
#define DMA_WRITE		CR_RD1
#define DMA_SEND_PACKET	(CR_RD1 | CR_RD0)
#define DMA_COMPLETE	CR_RD2

/* Command register Start|Stop Commands */
#define NIC_STOP		CR_STP
#define NIC_START		CR_STA
#define NIC_START_DMA_READ	(CR_STA | DMA_READ)
#define NIC_START_DMA_WRITE	(CR_STA | DMA_WRITE)

/* Interrupt status register bits. */
#define ISR_PRX 		0x01      /* Packet received */
#define ISR_PTX 		0x02      /* Packet transmitted */
#define ISR_RXE 		0x04      /* Receive error */
#define ISR_TXE 		0x08      /* Transmit error */
#define ISR_OVW 		0x10      /* Overwrite warning */
#define ISR_CNT 		0x20      /* Counter overflow */
#define ISR_RDC 		0x40      /* Remote DMA complete */
#define ISR_RST 		0x80      /* Reset status */

/* Interrupt mask register bits. */

#define IMR_PRXE 		0x01     /* Packet received interrupt enable */
#define IMR_PTXE 		0x02     /* Packet transmitted interrupt enable */
#define IMR_RXEE 		0x04     /* Receive error interrupt enable */
#define IMR_TXEE 		0x08     /* Transmit error interrupt enable */
#define IMR_OVWE 		0x10     /* Overwrite warning interrupt enable */
#define IMR_CNTE 		0x20     /* Counter overflow interrupt enable */
#define IMR_RCDE 		0x40     /* Remote DMA complete interrupt enable */

/* Data configuration register bits */

#define DCR_WTS 		0x01      /* Word transfer select */
#define DCR_BOS 		0x02      /* Byte order select */
#define DCR_LAS 		0x04      /* Long address select */
#define DCR_LS 			0x08       /* Loopback select */
#define DCR_AR 			0x10       /* Auto-initialize remote */
#define DCR_FT0 		0x20      /* FIFO threshold select bit 0 */
#define DCR_FT1 		0x40      /* FIFO threshold select bit 1 */

/* Transmit configuration register bits */

#define TCR_CRC 		0x01	/* Inhibit CRC */
#define TCR_LB0 		0x02	/* Encoded loopback control bit 0 */
#define TCR_LB1 		0x04	/* Encoded loopback control bit 1 */
#define TCR_ATD 		0x08	/* Auto transmit disable */
#define TCR_OFST 		0x10	/* Collision offset enable */

/* Transmit status register bits */

#define TSR_PTX 		0x01      /* Packet transmitted */
#define TSR_COL 		0x04      /* Transmit collided */
#define TSR_ABT 		0x08      /* Transmit aborted */
#define TSR_CRS 		0x10      /* Carrier sense lost */
#define TSR_FU 			0x20       /* FIFO underrun */
#define TSR_CDH 		0x40      /* CD heartbeat */
#define TSR_OWC 		0x80      /* Out of window collision */

/* Receive configuration register bits */

#define RCR_SEP 		0x01      /* Save errored packets */
#define RCR_AR 			0x02       /* Accept runt packets */
#define RCR_AB 			0x04       /* Accept broadcast */
#define RCR_AM 			0x08       /* Accept multicast */
#define RCR_PRO 		0x10      /* Promiscuous physical */
#define RCR_MON 		0x20      /* Monitor mode */

/* Receive status register bits */

#define RSR_PRX 		0x01      /* Packet received intact */
#define RSR_CRC 		0x02      /* CRC error */
#define RSR_FAE 		0x04      /* Frame alignment error */
#define RSR_FO 			0x08       /* FIFO overrun */
#define RSR_MPA 		0x10      /* Missed packet */
#define RSR_PHY 		0x20      /* Physical/multicast address */
#define RSR_DIS 		0x40      /* Receiver disabled */
#define RSR_DFR 		0x80      /* Deferring */

/* EEPROM command register bits */

#define EECR_EEM1  		0x80    /* EEPROM Operating Mode */
#define EECR_EEM0  		0x40    /* - 0 0 Normal operation
                                  		- 0 1 Auto-load
                                  		- 1 0 9346 programming
                                  		- 1 1 Config register write enab */
#define EECR_CS  		0x08    /* EEPROM Chip Select */
#define EECR_SK  		0x04    /* EEPROM Clock */
#define EECR_DI  		0x02    /* EEPROM Data In */
#define EECR_DO  		0x01    /* EEPROM Data Out */

#define RTL_EE_OPT_NORMAL		0x00
#define RTL_EE_OPT_AUTOLOAD		0x40
#define RTL_EE_OPT_PROGRAMM		0x80
#define RTL_EE_OPT_CONFIG_WR	0xC0

/* for programm operations */
#define EECR_CS_ON  		(EECR_CS | RTL_EE_OPT_PROGRAMM)
#define EECR_SK_ON  		(EECR_SK | RTL_EE_OPT_PROGRAMM)
#define EECR_DI_ON  		(EECR_DI | RTL_EE_OPT_PROGRAMM)
#define EECR_DO_ON  		(EECR_DO | RTL_EE_OPT_PROGRAMM)
#define EECR_CS_OFF 		(RTL_EE_OPT_PROGRAMM)
#define EECR_SK_OFF 		(RTL_EE_OPT_PROGRAMM)
#define EECR_DI_OFF 		(RTL_EE_OPT_PROGRAMM)
#define EECR_DO_OFF 		(RTL_EE_OPT_PROGRAMM)

/* Configuration register CONFIG0 bits */

#define CONFIG0_VID1	0x80	/* Version chip ID Hi Byte */
#define CONFIG0_VID0	0x40	/* Version chip ID Low Byte */
#define CONFIG0_AUI		0x20	/* Use AUI interface */
#define CONFIG0_PNPJP	0x10	/* PNP mode */
#define CONFIG0_JP		0x08	/* Jamper mode */
#define CONFIG0_BNC		0x04	/* BNC link */

/* Configuration register CONFIG1 bits */

#define CONFIG1_IRQEN	0x80

/* Configuration register CONFIG2 bits */

#define CONFIG2_PL1   	0x80 /* Network media type */
#define CONFIG2_PL0   	0x40 /* - 0 0 TP/CX auto-detect
                                  	- 0 1 10baseT
                                  	- 1 0 10base5
                                  	- 1 1 10base2 */
#define CONFIG2_BSELB 	0x20 /* BROM disable */
#define CONFIG2_BS4   	0x10 /* BROM size/base */
#define CONFIG2_BS3   	0x08
#define CONFIG2_BS2   	0x04
#define CONFIG2_BS1   	0x02
#define CONFIG2_BS0	0x01

/* Configuration register CONFIG3 bits */

#define CONFIG3_PNP     0x80 /* PnP Mode */
#define CONFIG3_FUDUP   0x40 /* Full duplex */
#define CONFIG3_LEDS1   0x20 /* LED1/2 pin configuration
                                        - 0 LED1 == LED_RX, LED2 == LED_TX
                                        - 1 LED1 == LED_CRS, LED2 == MCSB */
#define CONFIG3_LEDS0   0x10 /* LED0 pin configration
                                        - 0 LED0 pin == LED_COL
                                        - 1 LED0 pin == LED_LINK */
#define CONFIG3_SLEEP   0x04 /* Sleep mode */
#define CONFIG3_PWRDN   0x02 /* Power Down */
#define CONFIG3_ACTIVEB 0x01 /* inverse of bit 0 in PnP Act Reg */

/* Configuration register CONFIG4 bits */

#define CONFIG4_IOMS	0x01 /* full address decode */

/* EEPROM contens */
/*
typedef struct
{	
    // Power-up initial values of Page3 and PnP logical device configurations registers //
    UCHAR	Config0;
    UCHAR	Config1;
    UCHAR	Config2;
    UCHAR	Config3;
    UCHAR	Config4;

    // NE2000 IDPROM //
    UCHAR	EthernetID[6];	// Ethernet node address //
    UCHAR	ProductID[8];	// Assigned by card makers; negligible //

    // PnP Values //
    UCHAR	VendorID[4];
    UCHAR	SerialNuber[4];
    UCHAR	SerialIDChecksum;

} RTL_EE_t;
*/

/*****************************************************************************
RTL8019AS configuration
******************************************************************************/

/* data bus port */
#define RTL_DATA_PORT		PORTA
#define RTL_DATA_DDR		DDRA
#define RTL_DATA_PIN		PINA

/* hight byte address port */
#define RTL_HADDR_PORT		PORTC
#define RTL_HADDR_DDR		DDRC
#define RTL_HADDR_PIN		PINC

/* control port */
#define RTL_CTRL_PORT		PORTG
#define RTL_CTRL_DDR		DDRG
#define RTL_CTRL_PIN		PING
#define RTL_CTRL_PIN_WR		0		/* WR signal */
#define RTL_CTRL_PIN_RD		1		/* RD signal */

/* interrupt ports */
#define RTL_INT_PORT		PORTE
#define RTL_INT_DDR			DDRE
#define RTL_INT_PIN			PINE
#define RTL_INT_PIN_BIT		PE7

#define RTL_INT_VECTOR		INT7_vect	/* vector definition */
#define RTL_INT_CTRL_REG	EICRB	/* int. control register */
#define RTL_INT_CTRL_REG_B0	ISC70	/* setup bits in control register */
#define RTL_INT_CTRL_REG_B1	ISC71
#define RTL_INT_MASK_REG	EIMSK	/* int. mask register */
#define RTL_INT_MASK_BIT	INT7	/* int. mask bit */

/* enable RTL interrupt flags:
IMR_PRXE - Packet received interrupt enable
IMR_RXEE - Receive error interrupt enable
IMR_OVWE - Overwrite warning interrupt enable
IMR_CNTE - Counter overflow interrupt enable
IMR_TXEE - Transmit error interrupt enable
*/
#define RTL_INT_FLAGS_SETUP	(IMR_PRXE | IMR_RXEE | IMR_OVWE | IMR_CNTE)

/* base address */
#define RTL_BASE_ADDR		0x8000


/*****************************************************************************
driver interface
******************************************************************************/

//extern volatile UINT32 start_time;

/* Init module */
BOOL RTL_Init(UINT8 *mac);               // ����� ���������� ���. ������ ����� ������ 
                                         // �-��� ETH_LocalHostInit(&ETH_LocalHost, ETH_HOST_PARAMS_DEFAULT)
										 // ������ ������� - ��� ������ ���� ����, ��� � ������ ���� ������� �������� ��������
										 // � � ������ hw_addr[6] (��. ����) ������������ �������� 
/*
void ETH_LocalHostInit(Local_Host_t *host, UINT8 dir)
{
     if (dir == ETH_HOST_PARAMS_DEFAULT)
	 {	
            host->hw_addr[0] = '0';	// ZERO!!! 
			host->hw_addr[1] = 'F';
			host->hw_addr[2] = 'F';
			host->hw_addr[3] = 'I';
			host->hw_addr[4] = 'C';
			host->hw_addr[5] = 'E';
		
			host->ip_addr = MAKEUINT32(MAKEUINT16(99, 1), MAKEUINT16(168, 192));
			host->ip_mask = MAKEUINT32(MAKEUINT16(0, 255), MAKEUINT16(255, 255));
			host->ip_gateway = MAKEUINT32(MAKEUINT16(1, 1), MAKEUINT16(168, 192));
     }
}
*/										 // 
										 // � ���������� ��� �-��� ��� ����� �������:
                                         //    RTL_Init(ETH_LocalHost.hw_addr)
										 // ���-�� ���������, ���:
										 //      Local_Host_t ETH_LocalHost;
										 // � Local_Host_t, � ���� �������, ��������� ���
                                         //typedef struct
                                         //{	
                                         //    UINT8	hw_addr[ETH_HWA_LEN];
                                         //    UINT32	ip_addr;
                                         //    UINT32	ip_mask;
                                         //    UINT32	ip_gateway;
                                         //} Local_Host_t;
										 //
										 // � ��� ETH_HWA_LEN = 6, ��� ���������� � ����� "netbuf.h"




/* Receive NET packet */
UINT16 RTL_GetPacket(UINT8 *buf);        // ���������� �� ethernet.c, RTL8019.Driver.� (DBG_OUT)

/* Transmit NET packet */
//BOOL RTL_PutPacket(NET_Buffer_t *nbuf);  // ���������� �� ethernet.c, RTL8019.Driver.� (DBG_OUT)
BOOL RTL_PutPacket(UINT16 len, UINT8* packet);  // ���������� �� ethernet.c, RTL8019.Driver.� (DBG_OUT)
// enc28j60PacketSend(UINT16 len, UINT8* packet);


/* Check received packet */
//BOOL RTL_CheckReceived(void);          // ===000 ��� ���������� - ������ ���������. ???



/*****************************************************************************
******************************************************************************/
#endif /* RTL_8019AS_DRIVER_H */
