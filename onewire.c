#include "onewire.h"
#include "macrofun.h"


#define sbi(reg,bit) reg |= (1<<bit)
#define cbi(reg,bit) reg &= ~(1<<bit)
#define ibi(reg,bit) reg ^= (1<<bit)
#define CheckBit(reg,bit) (reg&(1<<bit))

extern unsigned char ds1820_rom_codes[MAX_DS1820][8];
extern void RunTasks(unsigned char tasks);


void OthersTasks(void){
//	RunTasks(0xFF);
}

void OW_Set(unsigned char mode)
{
	if (mode) {
		cbi(OW_PORT, OW_BIT); 
		sbi(OW_DDR, OW_BIT);
	}
	else {
		cbi(OW_PORT, OW_BIT); 
		cbi(OW_DDR, OW_BIT);
	}
}

unsigned char OW_CheckIn(void)
{
	return CheckBit(OW_PIN, OW_BIT);
}


unsigned char OW_Reset(void)
{
	unsigned char	status;
	OW_Set(1);
	_delay_us(480);
	OW_Set(0);
	_delay_us(60);
	//Store line value and wait until the completion of 480uS period
	status = OW_CheckIn();
	_delay_us(420);
	//Return the value read from the presence pulse (0=OK, 1=WRONG)
 return !status;
}

void OW_WriteBit(unsigned char bit)
{
	//Pull line low for 1uS
	OW_Set(1);
	_delay_us(1);
	//If we want to write 1, release the line (if not will keep low)
	if(bit) OW_Set(0); 
	//Wait for 60uS and release the line
	_delay_us(60);
	OW_Set(0);
}

unsigned char OW_ReadBit(void)
{
	unsigned char	bit=0;
	//Pull line low for 1uS
	OW_Set(1);
	_delay_us(1);
	//Release line and wait for 14uS
	OW_Set(0);
	_delay_us(14);
	//Read line value
	if(OW_CheckIn()) bit=1;
	//Wait for 45uS to end and return read value
	_delay_us(45);
	return bit;
}

void OW_WriteByte(unsigned char byte)
{
	unsigned char i;
	for (i=0; i<8; i++) OW_WriteBit(CheckBit(byte, i));
}

unsigned char OW_ReadByte(void)
{
	unsigned char n=0;
	unsigned char i;
	for (i=0; i<8; i++) if (OW_ReadBit()) sbi(n, i);
	
	return n;
}

unsigned char OW_SearchROM( unsigned char diff, unsigned char *id )
{ 	
	unsigned char i, j, next_diff;
	unsigned char b;

	if(!OW_Reset()) 
		return OW_PRESENCE_ERR;       // error, no device found

	OW_WriteByte(OW_CMD_SEARCHROM);     // ROM search command
	next_diff = OW_LAST_DEVICE;      // unchanged on last device
	
	i = OW_ROMCODE_SIZE * 8;         // 8 bytes
	do 
	{	
		j = 8;                        // 8 bits
		do 
		{ 
			b = OW_ReadBit();			// read bit
			if( OW_ReadBit() ) 
			{ // read complement bit
				if( b )                 // 11
				return OW_DATA_ERR;  // data error
			}
			else 
			{ 
				if( !b ) { // 00 = 2 devices
				if( diff > i || ((*id & 1) && diff != i) ) { 
						b = 1;               // now 1
						next_diff = i;       // next pass 0
					}
				}
			}
         OW_WriteBit( b );               // write bit
         *id >>= 1;
         if( b ) *id |= 0x80;			// store bit
         i--;
		} 
		while( --j );
		id++;                            // next byte
    } 
	while( i );
	return next_diff;                  // to continue search
}

void OW_FindROM(unsigned char *diff, unsigned char id[])
{
	while(1)
    {
		*diff = OW_SearchROM( *diff, &id[0] );
    	if ( *diff==OW_PRESENCE_ERR || *diff==OW_DATA_ERR ||
    		*diff == OW_LAST_DEVICE ) return;
    	//if ( id[0] == DS18B20_ID || id[0] == DS18S20_ID ) 
		return;
    }
}

unsigned char OW_ReadROM(unsigned char *buffer)
{
	unsigned char i;
	if (!OW_Reset()) return 0;
	OW_WriteByte(OW_CMD_READROM);
	for (i=0; i<8; i++)
	{
		buffer[i] = OW_ReadByte();
	}
 return 1;
}

unsigned char OW_MatchROM(unsigned char *rom)
{
	unsigned char i;
 	if (!OW_Reset()) return 0;
	OW_WriteByte(OW_CMD_MATCHROM);	
	for(i=0; i<8; i++)
	{
		OW_WriteByte(rom[i]);
	}
 return 1;
}


unsigned char search_ow_devices(void) // ����� ���� ��������� �� ����
{ 
	unsigned char	i;
   	unsigned char	id[OW_ROMCODE_SIZE];
   	unsigned char	diff, sensors_count;

	sensors_count = 0;

	for( diff = OW_SEARCH_FIRST; diff != OW_LAST_DEVICE && sensors_count < MAX_DS1820 ; )
    { 
		OW_FindROM( &diff, &id[0] );
      	if( diff == OW_PRESENCE_ERR ) break;
      	if( diff == OW_DATA_ERR )	break;
      	for (i=0;i<OW_ROMCODE_SIZE;i++)
         	ds1820_rom_codes[sensors_count][i] = id[i];
		sensors_count++;
    }
	return sensors_count;
}
