
#include <util/delay.h>
#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>


#include "rtc1307.h" 
#include "ds18x20.h"
#include "macrofun.h"
//#include "lcd_define_u.h"
//#include "lcd_wg_u.h"
//#include "GridCode.h"
#include "integer.h"
#include "ff.h"
#include "diskio.h"


//#define MAX_DS1820 8

extern long int temperature[MAX_DS1820];
extern unsigned char znak_t[MAX_DS1820];
extern unsigned char ds1820_devices;
extern unsigned char ds1820_rom_codes[MAX_DS1820][8];
extern unsigned char t_data[9]; // ���������� ��� �������� ����� SCRATCHPAD �������� �������
extern char sigt[2];


extern unsigned long int adc_press, pressure;
extern unsigned long int adc_humidity, humidity;
//extern unsigned char KeyScan, KeyPressed, F_KeyDown, KeyCode;

extern unsigned char HIST_BUFFER_SIZE;


extern unsigned char bars_P[24];
extern unsigned char bars_T0[24];
extern unsigned char bars_T1[24];
extern unsigned char bars_H[24];

extern unsigned char clock_scale[7];

//extern unsigned char DShour, DSmin, DSsec, DSday, DSmonth, DSyear;
extern unsigned char OldDSmin, OldDSsec, OldDShour, timechange;

extern unsigned char RTC_started;
extern RTC myclock;

//extern unsigned char grid[8][57];

extern unsigned char MenuItem;
//extern char param[3][3];
//extern char fooo_name[3][10];
//extern char menu_name[3][10];
//extern char par_name[3][10];
//extern char time_name[3][10];
//extern char cal_name[3][10];    

//extern char *str1, *str2, *str3, *str4, *str5, *str6, *str7, *str8; 
//extern char *cal_str, *tim_str, *t_ulica, *t_komn_, *h_komn_, *p_komn_;                     // 8 

extern char gradus, procent;

extern char fnamebuf[13];
extern char fn_year[4];
extern char fn_month[2];
extern char fn_day[2];
extern UINT16 ByteWrite;
extern UINT16 ByteRead;
extern UINT16 CountRead;
extern int fprintf_res;
extern int fread_res;

extern FRESULT f_err_code;
extern FATFS FATFS_Obj;
extern FIL fil_obj;


//extern void get_t_data(void);

//#pragma used+           


void InitPLC(void)
{
// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTA=0x00;
DDRA=0xFF;  

// Port B initialization
//
// FOR SPI -> Look in "SPI_init_M()" function...
//PORTB=0x08;
//DDRB=0xF7;


// Input/Output Ports initialization
// Port C initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTC=0x00;
DDRC=0xFF;  

// Port D initialization__________________________OK
// D0 = OUT = LED 0           = 0
// D1 = OUT = LED 1           = 0
// D2 = OUT = LED 2           = 0
// D3 = OUT = LED 3           = 0
// D4 = OUT = LED 4           = 0
// D5 = OUT/IN = 1-Wire       = 1
// D6 = IN  = BUTT SB1        = 1  PULL-UP
// D7 = IN  = BUTT SB2        = 1  PULL-UP
PORTD=0xE0;
DDRD=0x3F;


// Port E initialization__________________________OK
// E0 = OUT = TXD0             = 0
// E1 = IN  = RXD0       
// E2 = OUT = SCL I2C          = 0
// E3 = OUT = external WD reset 
//            from Timer3      = 0
// E4 = IN + PULL - input from keyboard
// E5 = IN + PULL - input from keyboard
// E6 = OUT/IN = SDA I2C       = 0
// E7 = IN + PULL - interrupt from Ethernet Chip
PORTE=0xB0;
DDRE=0x4D;


// Port G initialization
// Func4=In Func3=In Func2=Out Func1=Out Func0=Out 
// State4=T State3=T State2=0 State1=1 State0=1 
PORTG=0x03;
DDRG=0x07;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: 57,600 kHz
// Mode: Fast PWM top=FFh
// OC0 output: Non-Inverted PWM
ASSR=0x00;
TCCR0=0x6E;
TCNT0=0x00;
OCR0=0x80;

// Timer/Counter 3 initialization
// Clock source: System Clock
// Clock value: 230,400 kHz
// Mode: Normal top=FFFFh
// Noise Canceler: Off
// Input Capture on Falling Edge
// OC3A output: Toggle
// OC3B output: Discon.
// OC3C output: Discon.
// Timer 3 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
// Compare C Match Interrupt: Off
TCCR3A=0x40;
TCCR3B=0x03;
TCNT3H=0x00;
TCNT3L=0x00;
ICR3H=0x00;
ICR3L=0x00;
OCR3AH=0x00;
OCR3AL=0x00;
OCR3BH=0x00;
OCR3BL=0x00;
OCR3CH=0x00;
OCR3CL=0x00;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;
// ADC initialization
// ADC Clock frequency: 230,400 kHz
// ADC Voltage Reference: AVCC pin
ADMUX=ADC_VREF_TYPE & 0xff;

// ���������� �� ��� ���������
//ADCSRA=0x8E;
// ��� ���������� �� ���
ADCSRA=0x86;


// USART0 initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART0 Receiver: On
// USART0 Transmitter: On
// USART0 Mode: Asynchronous
// USART0 Baud Rate: 19200
UCSR0A=0x00;
UCSR0B=0x18;
UCSR0C=0x06;
UBRR0H=0x00;
UBRR0L=0x2F;


// I2C Bus initialization
//i2c_init();
//rtc_write(0,0);
// DS1307 Real Time Clock initialization
// Square wave output on pin SQW/OUT: On
// Square wave frequency: 1Hz
//rtc_init(0,1,0);           



}

//====== ��������� �������� ====================================================
// Read the AD conversion result - ��� ����������
unsigned int read_adc(unsigned char adc_input)
{
  ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
// Delay needed for the stabilization of the ADC input voltage
  _delay_us(10);
// Start the AD conversion
  ADCSRA|=0x40;
// Wait for the AD conversion to complete
  while ((ADCSRA & 0x10)==0);
  ADCSRA|=0x10;
  return ADCW;  // ��� ADC
}

void get_temp_data(void)
{
unsigned char i;
  ds1820_devices = search_ow_devices(); // ���� ��� 1-wire ����������
  if (ds1820_devices)
  {
    DS18x20_StartMeasure(); 
    _delay_ms(900); // ���� ������� 750 ��, ���� �������������� �����������
    for (i=0; i<ds1820_devices; i++)
    { 
	  DS18x20_ReadData(ds1820_rom_codes[ds1820_devices - 1 - i], t_data); // ��������� ������
	  _delay_ms(5);
	  temperature[i] = DS18x20_ConvertToThemperature(t_data, sigt); // ��������������� ����������� � ���������������� ���
	  znak_t[i] = sigt[0];
    }
  }
else
   {
   for (i=0; i<2; i++)
       {
       temperature[i] = 0;
       znak_t[i] = '+';
       }
   }   
}


unsigned char is_new_time(void)
{
unsigned char compatres = 0; 

 if (RTC_started) 
 {
   rtc_get_time(&myclock);
  if ((myclock.hour != OldDShour) & (myclock.hour < 24) & (myclock.min < 60) & (myclock.sec < 60)) 
  {   
//    if (myclock.hour == 0) 
//      compatres = 4;  // ����� �����
//    else
    compatres = 3;  // ����� ���
      
    OldDShour = myclock.hour; 
    OldDSmin = myclock.min; 
    OldDSsec = myclock.sec;
  }
  else
  {
    if ((myclock.min != OldDSmin) & (myclock.hour < 24) & (myclock.min < 60) & (myclock.sec < 60)) 
    {
      compatres = 2; 
      OldDSmin = myclock.min; 
      OldDSsec = myclock.sec;
    }
    else
    {
      if ((myclock.sec != OldDSsec) & (myclock.hour < 24) & (myclock.min < 60) & (myclock.sec < 60)) 
      {
        compatres = 1; 
        OldDSsec = myclock.sec;
      }
    }
  }
 } // if (RTC_started)      
return compatres;
}
  
void shiftbarsleft(unsigned char fill_newdata)
{   
unsigned char cntr;
int8_t v;
//
// ������� ��� ��������, �� ��������� �� 700 ��������.
// �������� ����� �� 1 �������...
 for (cntr = 0; cntr < HIST_BUFFER_SIZE; cntr++)  
 { bars_P[cntr] = bars_P[cntr+1]; }
//
// ������ ��� ����������� T0, �� ��������� �� +100 ��������
// �������� ����� �� 1 �������...
 for (cntr = 0; cntr < HIST_BUFFER_SIZE; cntr++)  
 { bars_T0[cntr] = bars_T0[cntr+1]; }
//
// ������ ��� ����������� T1, �� ��������� �� +100 ��������
// �������� ����� �� 1 �������...
 for (cntr = 0; cntr < HIST_BUFFER_SIZE; cntr++)  
 { bars_T1[cntr] = bars_T1[cntr+1]; }
//
// ���������.
// �������� ����� �� 1 �������...
 for (cntr = 0; cntr < HIST_BUFFER_SIZE; cntr++)  
 { bars_H[cntr] = bars_H[cntr+1]; }


// ������� �������� ������ ��� ����� �������
 clock_scale[6] = myclock.hour;
 for (cntr = 6; cntr > 0; cntr--) 
 {
   if (clock_scale[cntr] > 3)
     clock_scale[cntr - 1] = clock_scale[cntr] - 4;
   else
     clock_scale[cntr - 1] = clock_scale[cntr] + 20;
 }
  
// { if (clock_scale[cntr] == 23)
//    {clock_scale[cntr] = 0;}
//	else
//	{clock_scale[cntr] = clock_scale[cntr] + 1;}
// }

// � ����������� �� ����� - ����� ���� ����� (���������) ��������, ���� 
// - �������� �������� ����������� - ��������� ���� �������� ������
 if (fill_newdata == 1) 
 {
  // ... ���������� ������� �������� �������� �
   bars_P[HIST_BUFFER_SIZE] = (UINT8)(pressure - 700);	 
  // ... ���������� ������� �������� �0 � ��������� ������� �������:
  // - ��������� ������� � �������� �����...
   if (znak_t[0] == 0x2D) { v = -(int8_t)((temperature[0] + 5) / 10); }
   else { v = (int8_t)((temperature[0] + 5) / 10); }
   bars_T0[HIST_BUFFER_SIZE] = (UINT8)(100 + v);	        
  // ... ���������� - ��� ������ ����������� �1...
   if (znak_t[1] == 0x2D) { v = -(int8_t)((temperature[1] + 5) / 10); }
   else { v = (int8_t)((temperature[1] + 5) / 10); }
   bars_T1[HIST_BUFFER_SIZE] = (UINT8)(100 + v);	        
   // ... ���������� ������� �������� ��������� � (�� �� ������ 20%, ����� ������� ������ "���� ��������")
   if (humidity < 200)
     bars_H[HIST_BUFFER_SIZE] = (UINT8)(20);
   else
     bars_H[HIST_BUFFER_SIZE] = (UINT8)(humidity/10);	 
}
 else
 {
   bars_P[HIST_BUFFER_SIZE] = (UINT8)(10);	//������� ����������� 710��.��.��.
   bars_T0[HIST_BUFFER_SIZE] = (UINT8)(60);	//������� ����������� -40 ��������        
  // ... ���������� - ��� ������ ����������� �1...
   bars_T1[HIST_BUFFER_SIZE] = (UINT8)(60);	        
   bars_H[HIST_BUFFER_SIZE] = (UINT8)(20);	//������� ����������� 20% 
 }
}

// ====== �������� � ��������� ===========================================================
/*void get_press_hum()
{
unsigned long int adc_P, adc_H;
unsigned char i;
    // ������ ��� - ��� ����������, � ����������� 8 ��� 
    adc_P = 0;
    adc_H = 0;  
     
    for (i=0; i<8; i++)
    {
     adc_P = adc_P + read_adc(0);
     adc_H = adc_H + read_adc(1);
    }
    adc_press = (adc_P/8);
    adc_humidity = (adc_H/8); 
// �������������� �������� � ��������� � ����� ��������
   // mmHg x 1
   pressure = (((adc_press  + 1) * 49072 / 5025 + 950) / 9) * 76000 / 101325; 
   // % x 10
   humidity = ((adc_humidity + 1) * 4907 - 733400) / 3059;
}*/


/*void SendDataToPort(void)
{
	unsigned char i;
    printf("%02d.%02d.%d; %02d:%02d:%02d; ", myclock.mday, myclock.month, myclock.year, myclock.hour, myclock.min, myclock.sec);
	for (i=0; i<ds1820_devices; i++) 
	{
	  printf("T%d=%c%02d,%d%cC; ", (int)(i+1), znak_t[i], (int)(temperature[i]/10), (int)(temperature[i]%10), gradus);
	}
    printf ("P=%dmm; H=%02d,%d%c; \r\n", (int)pressure, (int)(humidity/10), (int)(humidity%10), procent);  
}*/

/*void form_fname (void)
{
  fn_year[2] = (myclock.year%100)/10 + 48;
  fn_year[3] = myclock.year%10 + 48;
  fn_month[0] = myclock.month/10 + 48;
  fn_month[1] = myclock.month%10 + 48;
//  fn_day[0] = myclock.mday/10 + 48;
//  fn_day[1] = myclock.mday%10 + 48;

  memcpy(fnamebuf, fn_year, 4); 
  memcpy(fnamebuf+6, fn_month, 2); 
//  memcpy(fnamebuf+6, fn_day, 2); 
  fnamebuf[12] = 0;
}*/

/*char get_fattime (void){
 return	((DWORD)(myclock.year-1980) << 25) 
      | ((DWORD)myclock.month << 21) 
	  | ((DWORD)myclock.mday << 16) 
	  | ((DWORD)myclock.hour << 11) 
	  | ((DWORD)myclock.min << 5) 
	  | ((DWORD)myclock.sec >> 1);
}



void SD_FS_Init(void)
{
    printf("\r\nStarting FATfs...\r\n");
    if ( ( disk_initialize(0) == 0) & ( f_mount(0, &FATFS_Obj) == 0) )
      {
	    f_err_code = f_open(&fil_obj, "index.htm", FA_READ);
		if (f_err_code == FR_NO_FILE)
	      {
	        f_err_code = f_open(&fil_obj, "index.htm", FA_CREATE_NEW);
	        f_close(&fil_obj); 
	      } // if (f_err_code == FR_NO_FILE)
        else
	      f_close(&fil_obj); 
        printf("FATfs OK!\n\r");
      } // if ( !( disk_initialize(0) ) & !( f_mount(0, &FATFS_Obj) ) )  
    else
      printf("FAT ERROR!\n\r");
}

void LogDataToFile(void)
{
  PORTD &= ~(0x10);    // RED    3 - writing to LOG-file   

  if ( ( disk_initialize(0) == 0) & ( f_mount(0, &FATFS_Obj) == 0) )
    {
      form_fname();
      f_err_code = f_open(&fil_obj, fnamebuf, FA_WRITE);
      if ( (f_err_code == FR_DISK_ERR) |  (f_err_code == FR_NO_FILESYSTEM))
         printf("; NO disk!\r\n");
      else
		{
		  if (f_err_code == FR_NO_FILE)
            {
              f_err_code = f_open(&fil_obj, fnamebuf, FA_CREATE_NEW);
              //printf("Creating file... Result = %d \r\n", (int)f_err_code);
              printf("Creating file... ");
              f_close(&fil_obj); 
              f_err_code = f_open(&fil_obj, fnamebuf, FA_WRITE);
            } // if (f_err_code == FR_NO_FILE)
          f_err_code = f_lseek(&fil_obj,fil_obj.fsize);
          printf("WR2F...");
          fprintf_res = f_printf (&fil_obj, 
                   "%02d.%02d.%d; %02d:%02d:%02d; %d; %c%02d,%d%cC; %c%02d,%d%cC; %d,%d%c; 0,0; 0,0;\n", 
                   myclock.mday, myclock.month, myclock.year,  
                   myclock.hour, myclock.min, myclock.sec,
                   (int)pressure, 
                   znak_t[0], (int)(temperature[0]/10), (int)(temperature[0]%10), gradus,
                   znak_t[1], (int)(temperature[1]/10), (int)(temperature[1]%10), gradus, 
				  (int)(humidity/10), (int)(humidity%10), procent );
	      f_close(&fil_obj); 
		  if (fprintf_res > 0) 
		    { 
			  printf("-> OK!\r\n"); 
			}
		  else printf("->ZERO\r\n");
        } // else (f_err_code == FR_DISK_ERR) | (f_err_code == FR_NO_FILESYSTEM)) 
	} // if ( !( disk_initialize(0) ) & !( f_mount(0, &FATFS_Obj) ) )  
  else
    { 
      printf("; FAT Error!\n\r");
    }

  PORTD |= 0x10;    // RED    3 - writing to LOG-file
}

//====== ������ � ������ ������  �� ����� ============================================//

void writehistoryfile(void)
{
UINT16 bytestodo, bytesdone;


  PORTD &= ~(0x08);    // YELLOW 4 - writing to HISTORY-file
   

  bytestodo = 107;
  if ( ( disk_initialize(0) == 0) & ( f_mount(0, &FATFS_Obj) == 0) )
    {
      f_err_code = f_open(&fil_obj, "history.dat", FA_WRITE);
      if ( (f_err_code == FR_DISK_ERR) |  (f_err_code == FR_NO_FILESYSTEM))
         printf("; NO disk!\r\n");
      else
		{
		  if (f_err_code == FR_NO_FILE)
            {
              f_err_code = f_open(&fil_obj, "history.dat", FA_CREATE_NEW);
              //printf("Creating file... Result = %d \r\n", (int)f_err_code);
              printf("Creating file... ");
              f_close(&fil_obj); 
              f_err_code = f_open(&fil_obj, "history.dat", FA_WRITE);
            } // if (f_err_code == FR_NO_FILE)
          f_err_code = f_lseek(&fil_obj,0);
          printf("WR2H...");
          // FRESULT f_write (FIL*, const void*, UINT16, UINT16*);	// Write data to a file
		  // FRESULT f_read (FIL*, void*, UINT16, UINT16*);			// Read data from a file
          f_write (&fil_obj, bars_P, bytestodo, &bytesdone);

	      f_close(&fil_obj); 
		  if (bytesdone == bytestodo) 
		    { 
			  printf("-> OK!\r\n"); 
			}
		  else printf("->ZERO\r\n");
        } // else (f_err_code == FR_DISK_ERR) | (f_err_code == FR_NO_FILESYSTEM)) 
	} // if ( !( disk_initialize(0) ) & !( f_mount(0, &FATFS_Obj) ) )  
  else
    { 
      printf("; FAT Error!\n\r");
    }

  PORTD |= 0x08;    // YELLOW 4 - writing to HISTORY-file

}

void readhistoryfile(void)
{
UINT16 bytestodo, bytesdone;

  PORTD &= ~(0x01);    // YELLOW 0 - reading to HISTORY-file

  bytestodo = 107;
  bytesdone = 0;

  if ( ( disk_initialize(0) == 0) & ( f_mount(0, &FATFS_Obj) == 0) )
    {
      f_err_code = f_open(&fil_obj, "history.dat", FA_READ);
      if ( (f_err_code == FR_DISK_ERR) |  (f_err_code == FR_NO_FILESYSTEM))
         printf("; NO disk!\r\n");
      else
		{
		  if (f_err_code == FR_NO_FILE)
            {
              printf("; NO file to read!\r\n");
            } // if (f_err_code == FR_NO_FILE)
          else
		    {
//              f_err_code = f_lseek(&fil_obj,0);
              printf("RD2H...");
              f_read (&fil_obj, bars_P, bytestodo, &bytesdone);
	          f_close(&fil_obj); 
			  printf("-> bytes read - %d\r\n", bytesdone); 
		      if (bytesdone == bytestodo) 
		        { 
			      printf("-> OK!\r\n"); 
			    }
		      else printf("->ZERO\r\n");
		  }
        } // else (f_err_code == FR_DISK_ERR) | (f_err_code == FR_NO_FILESYSTEM)) 
	} // if ( !( disk_initialize(0) ) & !( f_mount(0, &FATFS_Obj) ) )  
  else
    { 
      printf("; FAT Error!\n\r");
    }
  PORTD |= 0x01;    // YELLOW 0 - reading to HISTORY-file
}*/

 
//#pragma used- 
