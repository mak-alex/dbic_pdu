/*--------------------------------------------------------------------------*/
/*  RTC controls                                                            */

#include <avr/io.h>
#include <string.h>
#include "rtc1307.h"



#define SCL_LOW()	DDRE |=	0x04			/* SCL = LOW */
#define SCL_HIGH()	DDRE &=	0xFB			/* SCL = High-Z */
#define	SCL_VAL		((PINE & 0x04) ? 1 : 0)	/* SCL input level */
#define SDA_LOW()	DDRE |=	0x40			/* SDA = LOW */
#define SDA_HIGH()	DDRE &=	0xBF			/* SDA = High-Z */
#define	SDA_VAL		((PINE & 0x40) ? 1 : 0)	/* SDA input level */



/*-------------------------------------------------*/
/* I2C bus protocol                                */
static
void iic_delay (void)
{
	int n;
	for (n = 12; n; n--) PINE;
}
/* Generate start condition on the IIC bus */
static
void i2c_start (void)
{
	SDA_HIGH();
	iic_delay();
	SCL_HIGH();
	iic_delay();
	SDA_LOW();
	iic_delay();
	SCL_LOW();
	iic_delay();
}
/* Generate stop condition on the IIC bus */
static
void i2c_stop (void)
{
	SDA_LOW();
	iic_delay();
	SCL_HIGH();
	iic_delay();
	SDA_HIGH();
	iic_delay();
}
/* Send a byte to the IIC bus */
static
int i2c_write (unsigned char dat)
{
	unsigned char b = 0x80;
	int ack;

	do {
		if (dat & b)	 {	/* SDA = Z/L */
			SDA_HIGH();
		} else {
			SDA_LOW();
		}
		iic_delay();
		SCL_HIGH();
		iic_delay();
		SCL_LOW();
		iic_delay();
	} while (b >>= 1);
	SDA_HIGH();
	iic_delay();
	SCL_HIGH();
	ack = SDA_VAL ? 0 : 1;	/* Sample ACK */
	iic_delay();
	SCL_LOW();
	iic_delay();
	return ack;
}
/* Receive a byte from the IIC bus */
static
unsigned char i2c_read (unsigned char ack)
{
	UINT16 d = 1;

	do {
		d <<= 1;
		SCL_HIGH();
		if (SDA_VAL) d++;
		iic_delay();
		SCL_LOW();
		iic_delay();
	} while (d < 0x100);
	if (ack) {		/* SDA = ACK */
		SDA_LOW();
	} else {
		SDA_HIGH();
	}
	iic_delay();
	SCL_HIGH();
	iic_delay();
	SCL_LOW();
	SDA_HIGH();
	iic_delay();

	return (unsigned char)d;
}


/*-------------------------------------------------*/
/* RTC functions                                   */
unsigned char rtc_read(unsigned char address)
{
unsigned char data;

 i2c_start();
 i2c_write(0xd0);
 i2c_write(address);
 i2c_start();
 i2c_write(0xd1);
 data=i2c_read(0);
 i2c_stop();
 return data;
}

void rtc_write(unsigned char address,unsigned char data)
{
 i2c_start();
 i2c_write(0xd0);
 i2c_write(address);
 i2c_write(data);
 i2c_stop();
}

void rtc_init(unsigned char rs,unsigned char sqwe,unsigned char out)
{
 rs&=3;
 if (sqwe) rs|=0x10;
 if (out) rs|=0x80;
 i2c_start();
 i2c_write(0xd0);
 i2c_write(7);
 i2c_write(rs);
 i2c_stop();
}

//void rtc_get_time(unsigned char *hour,unsigned char *min,unsigned char *sec)
void rtc_get_time(RTC *rtc)
{
unsigned char buf[8];

 i2c_start();
 i2c_write(0xd0);
 i2c_write(0);
 i2c_start();
 i2c_write(0xd1);
 /* *sec */   buf[0]=i2c_read(1);
 /* *min */   buf[1]=i2c_read(1);
 /* *hour */  buf[2]=i2c_read(0);
 rtc->sec = (buf[0] & 0x0F) + ((buf[0] >> 4) & 7) * 10;
 rtc->min = (buf[1] & 0x0F) + (buf[1] >> 4) * 10;
 rtc->hour = (buf[2] & 0x0F) + ((buf[2] >> 4) & 3) * 10;
 i2c_stop();
}

//void rtc_get_date(unsigned char *date,unsigned char *month,unsigned char *year)
void rtc_get_date(RTC *rtc)
{
unsigned char buf[8];

 i2c_start();
 i2c_write(0xd0);
 i2c_write(3);
 i2c_start();
 i2c_write(0xd1);
 /* *wday */  buf[3]=i2c_read(1);
 /* *date */  buf[4]=i2c_read(1);
 /* *month */ buf[5]=i2c_read(1);
 /* *year */  buf[6]=i2c_read(0);
 rtc->wday = buf[3] & 0x07;
 rtc->mday = (buf[4] & 0x0F) + ((buf[4] >> 4) & 3) * 10;
 rtc->month = (buf[5] & 0x0F) + ((buf[5] >> 4) & 1) * 10;
 rtc->year = 2000 + (buf[6] & 0x0F) + (buf[6] >> 4) * 10;
 i2c_stop();
}


void rtc_set_time(const RTC *rtc)
{
unsigned char buf[8];

 buf[0] = rtc->sec / 10 * 16 + rtc->sec % 10;
 buf[1] = rtc->min / 10 * 16 + rtc->min % 10;
 buf[2] = rtc->hour / 10 * 16 + rtc->hour % 10;
 i2c_start();
 i2c_write(0xd0);
 i2c_write(0);
 i2c_write(buf[0]);
 i2c_write(buf[1]);
 i2c_write(buf[2]);
 i2c_stop();
}

void rtc_set_date(const RTC *rtc)
{
unsigned char buf[8];

 buf[3] = rtc->wday & 0x07;
 buf[4] = rtc->mday / 10 * 16 + rtc->mday % 10;
 buf[5] = rtc->month / 10 * 16 + rtc->month % 10;
 buf[6] = (rtc->year - 2000) / 10 * 16 + (rtc->year - 2000) % 10;
 i2c_start();
 i2c_write(0xd0);
 i2c_write(3);
 i2c_write(buf[3]);
 i2c_write(buf[4]);
 i2c_write(buf[5]);
 i2c_write(buf[6]);
 i2c_stop();
}



