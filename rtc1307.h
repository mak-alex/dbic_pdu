#include "integer.h"

typedef struct {
	WORD	year;	/* 2000..2099 */
	unsigned char	month;	/* 1..12 */
	unsigned char	mday;	/* 1.. 31 */
	unsigned char	wday;	/* 1..7 */
	unsigned char	hour;	/* 0..23 */
	unsigned char	min;	/* 0..59 */
	unsigned char	sec;	/* 0..59 */
} RTC;


/* Initialize DS1307 device*/
void rtc_init(unsigned char rs,unsigned char sqwe,unsigned char out);
/* Read from DS1307 device */
unsigned char rtc_read(unsigned char address);
/* Write to DS1307 device */
void rtc_write(unsigned char address,unsigned char data);
/* Get time */
void rtc_get_time(RTC *rtc);
/* Get date */
void rtc_get_date(RTC *rtc);
/* Set time */
void rtc_set_time(const RTC *rtc);
/* Set date */
void rtc_set_date(const RTC *rtc);
