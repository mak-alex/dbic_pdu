/*****************************************************************************
����: XMEM.c
Created by PROTTOSS  mail to PROTTOSS@mail.ru
XMEM routine 05.06.2007
******************************************************************************/


#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <util/delay.h>



//#include <intrinsics.h>
#include "integer.h"

//#include "stdafx.h"
#include "XMEM.h"

//#define DBG_XMEM

#define XMEM_TEST_ENABLE

#define NO_PROTEUS_SIMUL

/*****************************************************************************
debug messages
******************************************************************************/

#ifdef DBG_XMEM
#define DBG_OUT(...)	printf(__VA_ARGS__);
#else
#define DBG_OUT(...);
#endif	/* #ifdef DBG_IP */

/*****************************************************************************
Init XMEM unit
******************************************************************************/
char XMEM_Init(void)
{	
  	/* include pull-ups in all used ports*/
    XMEM_DATA_PORT = 0xff;
    XMEM_HADDR_PORT = 0xff;
    XMEM_CTRL_PORT |= ((1 << XMEM_CTRL_PIN_WR)|(1 << XMEM_CTRL_PIN_RD)|(1 << XMEM_CTRL_PIN_ALE));

    /* enable XMEM and setup wait cycles */
    MCUCR = (1 << SRE);
    XMCRA = XMEM_WAIT_CUCLES;

    /* wait for chip stub */
  	_delay_ms(50);
	
    return 1;	
}


/*****************************************************************************
Physical test of XMEM area 0x1100 - 0x7FFF
******************************************************************************/
char XMEM_test7FFF(void)
{	
#ifdef XMEM_TEST_ENABLE
    UCHAR patterns[] = { 0x00, 0xff, 0x55, 0xaa,};
    for(UCHAR pi = 0; pi < sizeof(patterns); pi++)
    {
      	DBG_OUT("Testing pattern 0x%02x...\n\r", patterns[pi]);
		for(UINT16 addr = XMEM_START; addr <= XMEM_END; addr++)
    	{	
      		UCHAR temp = *((UCHAR*)addr);
          	*((UCHAR*)addr) = patterns[pi];
            UCHAR res = *((UCHAR*)addr);
            *((UCHAR*)addr) = temp;
        	if(patterns[pi] != res)
            {	
              	DBG_OUT("XMEM_Init error: address: %d - need to be %d, is - %d\n\r", addr, patterns[pi], res);
               	return 0;
            }
        }
    }
#endif	/* #ifdef XMEM_TEST_ENABLE */
    /* OK! */
    return 1;	
}


/*****************************************************************************
Test of XMEM by allocation procedures
******************************************************************************/
char XMEM_alloc(void)
{	
#ifdef XMEM_TEST_ENABLE

    WORD *xmembuf1, *xmembuf2, *xmembuf3;
	xmembuf1 = malloc(BUFFER_SIZE);
	WORD index;
	WORD data=0;
	DBG_OUT("Allocating %d-byte's buffers in external RAM. \r\n",BUFFER_SIZE);   
	DBG_OUT("1-st starting at 0x%04X filled with:\r\n",(unsigned int)xmembuf1);
	// Fill memory incrementing values
	for(index = 0; index < BUFFER_SIZE/2; index++)
	{
    	xmembuf1[index] = data++;
	}
	// Display memory block
	for(index = 0; index < BUFFER_SIZE/2; index++)
	{
		DBG_OUT("%03X ",xmembuf1[index]);
		if((index&0x0F) == 0x0F)
		{
			DBG_OUT("\r\n");
		}
	}


    data = 0x03FF;
	xmembuf2 = malloc(BUFFER_SIZE);
	DBG_OUT("2-nd starting at 0x%04X filled with:\r\n",(unsigned int)xmembuf2);
	// Fill memory incrementing values
	for(index = 0; index < BUFFER_SIZE/2; index++)
	{
    	xmembuf2[index] = data--;
	}
	// Display memory block
	for(index = 0; index < BUFFER_SIZE/2; index++)
	{
		DBG_OUT("%03X ",xmembuf2[index]);
		if((index&0x0F) == 0x0F)
		{
			DBG_OUT("\r\n");
		}
	}
	//free allocated memory
#ifdef NO_PROTEUS_SIMUL
	free(xmembuf1);
#endif // #ifdef NO_PROTEUS_SIMUL

    data = 0x01FF;
	xmembuf3 = malloc(BUFFER_SIZE/2);
	DBG_OUT("3-rd starting at 0x%04X filled with:\r\n",(unsigned int)xmembuf3);
	// Fill memory incrementing values
	for(index = 0; index < BUFFER_SIZE/4; index++)
	{
    	xmembuf3[index] = data--;
	}
	// Display memory block
	for(index = 0; index < BUFFER_SIZE/4; index++)
	{
		DBG_OUT("%03X ",xmembuf3[index]);
		if((index&0x0F) == 0x0F)
		{
			DBG_OUT("\r\n");
		}
	}
	//free allocated memory
#ifdef NO_PROTEUS_SIMUL
	free(xmembuf3);
	free(xmembuf2);
#endif // #ifdef NO_PROTEUS_SIMUL
	DBG_OUT("\r\nAllocated memory is free!\r\n");   

#endif	/* #ifdef XMEM_TEST_ENABLE */

    /* OK! */
    return 1;	
}

