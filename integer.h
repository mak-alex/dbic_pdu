/*-------------------------------------------*/
/* Integer type definitions for FatFs module */
/*-------------------------------------------*/

#ifndef _INTEGER
#define _INTEGER

#if 0
#include <windows.h>
#else




/* These types must be 16-bit, 32-bit or larger integer */
//typedef int				INT;
//typedef unsigned int		UINT;


/* These types must be 8-bit integer */
typedef char			CHAR;
typedef signed char		SHAR;
typedef unsigned char	UCHAR;
typedef unsigned char	BYTE;
#define INT8			CHAR
#define UINT8			UCHAR
#define SINT8			SCHAR
#define MAXUINT8		((UINT8)(-1))

/* These types must be 16-bit integer */
typedef short			SHORT;
typedef unsigned short	USHORT;
typedef unsigned short	WORD;
typedef unsigned short	WCHAR;
typedef int          	INT16;
typedef signed int		SINT16;
typedef unsigned int	UINT16;
#define MAXUINT16		((UINT16)(-1))

/* These types must be 32-bit integer */
typedef long			LONG;
typedef unsigned long	ULONG;
typedef unsigned long	DWORD;
typedef signed long		SINT32;
#define INT32			LONG
#define UINT32			ULONG
#define MAXUINT32		((UINT32)(-1))

/* These types must be 32-bit integer */
typedef long long			INT64;
typedef unsigned long long	UINT64;
typedef signed long	long	SINT64;


/* Boolean type */
//typedef enum { FALSE = 0, TRUE } BOOL;

/* boolean */
#define BOOL	UCHAR
#define FALSE	0
#define TRUE	1

/* utils */
#define MAKEUINT16(byte_h, byte_l)	((UINT16)(((UINT16)byte_h << 8)|(UINT16)byte_l))
#define HIBYTE(word)				((UCHAR)((UINT16)word >> 8))
#define LOBYTE(word)				((UCHAR)((UINT16)word & 0xff))

#define MAKEUINT32(word_h, word_l)	((UINT32)(((UINT32)word_h << 16)|(UINT32)word_l))
#define HIWORD(longv)				((UINT16)((UINT32)longv >> 16))
#define LOWORD(longv)				((UINT16)((UINT32)longv & 0xffff))

#define SWAP16(n16)					(MAKEUINT16(LOBYTE(n16), HIBYTE(n16)))
#define SWAP32(n32)					(MAKEUINT32(SWAP16(LOWORD(n32)), SWAP16(HIWORD(n32))))


#endif

#endif
