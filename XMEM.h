/*****************************************************************************
����: XMEM.h
Creted by PROTTOSS
Mail to PROTTOSS@mail.ru

XMEM header

05.06.2007
******************************************************************************/

#ifndef XMEM_H
#define XMEM_H

#include <avr/io.h>
/*****************************************************************************
External SRAM configuration
******************************************************************************/

/* enable XMEM start test */
//#define XMEM_TEST_ENABLE
#define BUFFER_SIZE 512


/* port definitions */

/* data port */
#define XMEM_DATA_PORT		PORTA
#define XMEM_DATA_DDR		DDRA
#define XMEM_DATA_PIN		PINA

/* hight byte address port */
#define XMEM_HADDR_PORT		PORTC
#define XMEM_HADDR_DDR		DDRC
#define XMEM_HADDR_PIN		PINC

/* control port */
#define XMEM_CTRL_PORT		PORTG
#define XMEM_CTRL_DDR		DDRG
#define XMEM_CTRL_PIN		PING
#define XMEM_CTRL_PIN_WR	0		/* WR signal */
#define XMEM_CTRL_PIN_RD	1		/* RD signal */
#define XMEM_CTRL_PIN_ALE	2		/* ALE signal */

/* address space */
#define XMEM_START			0x1100
#define XMEM_END			0x7fff	/* 32 Kbytes */

/* setup */
#define XMEM_WAIT_CUCLES	((1 << SRW00) | (1 << SRW01))



/* Init XMEM unit */
char XMEM_Init(void);
char XMEM_test7FFF(void);
char XMEM_alloc(void);



#endif /*  #ifndef XMEM_H */
