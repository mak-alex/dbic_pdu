# DBIC-PDU
** Remotely controlled sockets AVR ATmega128
-----------------------------------
### Dimmer with a web interface for 8 channels, opportunities
* 8 sockets management via web interface
* The opportunity to sign the required outlet
* Formation of logs in three variants
* 8 sockets management console UDP
* Remembers the state in the absence of the input voltage
-----------------------------------
# About
### Hardware
    ATmega 128
    Ferrite ring
    Solid State Relays
-----------------------------------
# Configuration
### Pins configuration
    #define PIN_COUNT       17
    #define IO_INPUT        1
    #define IO_INPUT_ADC    2
    #define IO_OUT          3

    // PIN Config Array
    // 0 - not used
    // 1 (IO_INPUT) - Input
    // 2 (IO_INPUT_ADC) - Analog Input
    // 3 (IO_OUT) - Otput
    uint8_t IO_CFG[PIN_COUNT] = {IO_OUT,IO_OUT,IO_OUT,IO_OUT,IO_OUT,IO_OUT,IO_OUT,IO_OUT,IO_OUT,IO_INPUT_ADC,IO_INPUT_ADC,IO_INPUT_ADC,IO_INPUT_ADC,IO_INPUT_ADC,IO_INPUT_ADC,IO_INPUT_ADC,IO_INPUT_ADC};
    /*
     default values Array
    */
    uint8_t IO_DEFAULT[PIN_COUNT] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
-----------------------------------
### Open main.c and search string:
	/*
        CONFIGURED PORT
    */
-----------------------------------
### Example configure port C in output & D in input:
    void PIN_SET_OUT(uint8_t Pin){
        switch (Pin)
        {
            case 0: DDRC|=(1<<DDC0); break; 
            case 1: DDRC|=(1<<DDC1); break;
            case 2: DDRC|=(1<<DDC2); break; 
            case 3: DDRC|=(1<<DDC3); break;
            case 4: DDRC|=(1<<DDC4); break; 
            case 5: DDRC|=(1<<DDC5); break;
            case 6: DDRC|=(1<<DDC6); break; 
            case 7: DDRC|=(1<<DDC7); break;
            case 8: DDRC=0xFF; break;
        }
    }

    /*
     SET PIN IN
    */
    void PIN_SET_IN(uint8_t Pin){
        switch (Pin)
        {
            case 9: DDRF&=~(1<<DDF0); PORTF|=(1<<PORTF0);  break; 
            case 10: DDRF&=~(1<<DDF1); PORTF|=(1<<PORTF1); break;
            case 11: DDRF&=~(1<<DDF2); PORTF|=(1<<PORTF2); break; 
            case 12: DDRF&=~(1<<DDF3); PORTF|=(1<<PORTF3); break;
            case 13: DDRF&=~(1<<DDF4); PORTF|=(1<<PORTF4); break; 
            case 14: DDRF&=~(1<<DDF5); PORTF|=(1<<PORTF5); break;
            case 15: DDRF&=~(1<<DDF6); PORTF|=(1<<PORTF6); break; 
            case 16: DDRF&=~(1<<DDF7); PORTF|=(1<<PORTF7); break;
        }
    }

    /*
     SET PIN IN ADC
    */
    void PIN_SET_IN_ADC(uint8_t Pin){
    //    PIN_SET_IN (Pin);
    }

    /*
     SET PIN ON
    */
    void PIN_SET_ON(uint8_t Pin){
        int i;
        if (IO_CFG[Pin]==IO_OUT) {
            switch (Pin)
            {
            case 0: PORTC&=~(1<<0); break; 
            case 1: PORTC&=~(1<<1); break;
            case 2: PORTC&=~(1<<2); break; 
            case 3: PORTC&=~(1<<3); break;
            case 4: PORTC&=~(1<<4); break; 
            case 5: PORTC&=~(1<<5); break;
            case 6: PORTC&=~(1<<6); break; 
            case 7: PORTC&=~(1<<7); break;
            case 8: for(i=0;i<=7;i++) { PORTC&=~(1<<i); _delay_ms(250); }; break;
            }
        }
    }


    /*
     SET PIN OFF
    */
    void PIN_SET_OFF(uint8_t Pin){
        int i;
        if (IO_CFG[Pin]==IO_OUT) {
            switch (Pin)
            {
            case 0: PORTC|=(1<<0); break; 
            case 1: PORTC|=(1<<1); break;
            case 2: PORTC|=(1<<2); break; 
            case 3: PORTC|=(1<<3); break;
            case 4: PORTC|=(1<<4); break; 
            case 5: PORTC|=(1<<5); break;
            case 6: PORTC|=(1<<6); break; 
            case 7: PORTC|=(1<<7); break;
            case 8: for(i=0;i<=7;i++) { PORTC|=(1<<i); }; break;
            }
        }
    }

    /*
     SET PIN ADC
    */
    uint16_t PIN_GET_ADC(uint8_t Pin){
        //if (IO_CFG[Pin]==IO_INPUT_ADC) {
            switch (Pin)
            {
                case 9: return adc_read(0); break; 
                case 10: return adc_read(1);  break;
                case 11: return adc_read(2);  break;    
                case 12: return adc_read(3);  break;
                case 13: return adc_read(4);  break;    
                case 14: return adc_read(5);  break;
                case 15: return adc_read(6);  break;    
                case 16:  return adc_read(7);  break;
            }
    //  }
        return 0;
    }

    /*
     GET: PIN VALUE
    */
    uint16_t PIN_GET_VALUE(uint8_t Pin){
        if (IO_CFG[Pin]==IO_INPUT_ADC) {
            switch (Pin)
            {
                case 9:  return PIN_GET_ADC(Pin); 
                case 10: return PIN_GET_ADC(Pin);
                case 11: return PIN_GET_ADC(Pin); 
                case 12: return PIN_GET_ADC(Pin);
                case 13: return PIN_GET_ADC(Pin); 
                case 14: return PIN_GET_ADC(Pin);
                case 15: return PIN_GET_ADC(Pin); 
                case 16: return PIN_GET_ADC(Pin);

            }
        }

        
        if (IO_CFG[Pin]==IO_INPUT) {
            switch (Pin)
            {
                case 9:  return ((PINF & (1<<PINF0))==(1<<PINF0)); break; 
                case 10: return ((PINF & (1<<PINF1))==(1<<PINF1)); break;
                case 11: return ((PINF & (1<<PINF2))==(1<<PINF2)); break; 
                case 12: return ((PINF & (1<<PINF3))==(1<<PINF3)); break;
                case 13: return ((PINF & (1<<PINF4))==(1<<PINF4)); break; 
                case 14: return ((PINF & (1<<PINF5))==(1<<PINF5)); break;
                case 15: return ((PINF & (1<<PINF6))==(1<<PINF6)); break; 
                case 16: return ((PINF & (1<<PINF7))==(1<<PINF7)); break;
            }
        }

        
        if (IO_CFG[Pin]==IO_OUT) {
            switch (Pin)
            {
                case 0: return ((PORTC & (1<<PORTC0))==(1<<PORTC0)); break;
                case 1: return ((PORTC & (1<<PORTC1))==(1<<PORTC1)); break;
                case 2: return ((PORTC & (1<<PORTC2))==(1<<PORTC2)); break;
                case 3: return ((PORTC & (1<<PORTC3))==(1<<PORTC3)); break;
                case 4: return ((PORTC & (1<<PORTC4))==(1<<PORTC4)); break;
                case 5: return ((PORTC & (1<<PORTC5))==(1<<PORTC5)); break;
                case 6: return ((PORTC & (1<<PORTC6))==(1<<PORTC6)); break;
                case 7: return ((PORTC & (1<<PORTC7))==(1<<PORTC7)); break;
                case 8: return ((PORTC & (1<<0x00))==(1<<0x00)); break;
            }
        }
        return -1;
    }
-----------------------------------
### Example configure network settings:
    // Default IP address
    if (result == 4) {
        netsettings.myip[0] = 10;
        netsettings.myip[1] = 1;
        netsettings.myip[2] = 1;
        netsettings.myip[3] = 244;
    }
    //Default Mask
    result = 0;
    for (i=0; i < 4; i++) {
        if (netsettings.mask[i] == 0xFF)
            result++;
    }
    if (result == 4) {
        netsettings.mask[0] = 255;
        netsettings.mask[1] = 255;
        netsettings.mask[2] = 255;
        netsettings.mask[3] = 0;
    }

    //Default Gateway
    result = 0;
    for (i=0; i < 4; i++) {
        if (netsettings.gateway[i] == 0xFF)
            result++;
    }
    if (result == 4) {
        netsettings.gateway[0] = 10;
        netsettings.gateway[1] = 1;
        netsettings.gateway[2] = 1;
        netsettings.gateway[3] = 1;
    }
-----------------------------------
### Example configure device settings:
    // Password device
    if ((uint8_t)netsettings.password[i] == 0xFF) {
        strncpy (netsettings.password,"password\0",9);
    }
    // Language device // 0 - kz // 1 - ru // 2 - en
    if (netsettings.lang == 0xFF) {
        netsettings.lang = 0;
    }
-----------------------------------
### HTTP/GET API
	http://[ip address your device]/
-----------------------------------
# UART Configuration and manipulation
### PARSE COMMAND
    /*
     UART: PARSE COMMAND
    */
    uint8_t UART_COMAND (uint8_t *Ucmd, uint8_t Ulen) {
        strncpy (Strbuf,RXBuffer,Ulen);
        Strbuf[Ulen]='\0';
        strupr(Strbuf);
        return (strcmp(Strbuf,Ucmd) == 0);
    }
-----------------------------------
### List commands UART
    /*
     UART: COMMAND TO MANIPULATION
    */
    void UART_Command(void) {
        char n, s;
        uint8_t i,j,k=9;//, n=0;
        uint8_t RXbyte[6];
        float VATT;
        
        uart1_puts_p(FS_RN);

        // AT
        if (UART_COMAND("AT", 2)) {
            uart1_puts_p(FS_OK);
        }

        //CONFIG
        if (UART_COMAND("CONFIG", 6)) {
            uart1_puts_p(PSTR("MAC:"));
            for (i=0;i<6;i++) {
                intToStr(Strbuf, netsettings.mymac[i], 3);
                uart1_puts(Strbuf);
                if (i<5) {
                    uart_putc('-');
                }
            }
    ...
-----------------------------------
# More functions
### Check status power
    /*
     CHECK FUNCTIONS
     ******************
     CHECK STATUS POWER
    */
    void check_ADC(void) {
        unsigned int i;
        
        for(i=0;i<=7;i++){
            
            if(!(adc_read(i)>1023)) {
                if(PORTC&~(1<<i));
                } else {
                    //PORTC&=~(1<<i);
                    _delay_ms(250);
                    PIN_SET_OFF(i);
            }
        }
    }
-----------------------------------
# Check status outlets: on or off
    /*
     CHECK FUNCTIONS
     **********************
     CHECK OFF STATUS TO ON
    */
    void check_SOCKET(void){
        int i;
        event_line event;
        
        for (i=0;i<=7;i++) {
            _delay_ms(500);
            //if((netsettings.interval > 0)&&(netsettings.interval)) {
                eeprom_read_block(&event, &EEMEM_EVENTLIST[i], sizeof(event));
                if(event.portvalue == 1) {
                    PIN_SET_ON(event.portpin);
                }/* else {
                    PIN_SET_ON(event.portpin);
                }*/
            //i++;
            //}
        }       
    } //
-----------------------------------
Screenshots
-------------
![Alt text](https://bytebucket.org/d_e_n_i_e_d/dbic_pdu/raw/ca1224c2b5ba8b129d6004ad1ec5d6a509f2941c/screenshots/main_window.png?token=52bb4570c221ede6ae0e33aa3e5a75957499692e)
![Alt text](https://bytebucket.org/d_e_n_i_e_d/dbic_pdu/raw/ae52625dc92cc9a09619d1d326143ada7c2d9f5f/screenshots/settings_window.png?token=4af5ad44a775573dce3d93438f9d0d85349f6b1b)
![Alt text](https://bytebucket.org/d_e_n_i_e_d/dbic_pdu/raw/ae52625dc92cc9a09619d1d326143ada7c2d9f5f/screenshots/statexml_window.png?token=dcb17eae04d45b72f99b59194cecead448a1e25f)
![Alt text](https://bytebucket.org/d_e_n_i_e_d/dbic_pdu/raw/ae52625dc92cc9a09619d1d326143ada7c2d9f5f/screenshots/titles_window.png?token=7d78042ebff7a34676126df8805258dad995a598)