-- vim: softtabstop=2:tabstop=2:shiftwidth=2:expandtab:retab
-- testZAPI - test the zabbixapi.lua
-- Author: Pavel Kraynyukhov
-- 
--[[
	Globakl Lua path
]]--
package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";

local MCASTMUX_HOME=os.getenv("MCASTMUX_HOME");
local lscript_path=MCASTMUX_HOME.."/lua"
local JSONf = assert(loadfile(lscript_path.."/JSON.lua"));
local JSON=JSONf()
local graph = require("graph");
local zapi=require("zapi");
require("LuaXml")
local XML = xml
local soap=require("CMDBuildSOAPAPI")

local base_url="http://10.1.1.13/zabbix/api_jsonrpc.php"	  -- Test
--local base_url="http://10.244.244.133/zabbix/api_jsonrpc.php" -- Release

local WS_URL="http://10.244.244.149:8080/cmdbuild/services/soap/Webservices" -- Test

local function GetSOAPReturn(xmlresp)
  local xmltab=XML.eval(xmlresp)
  local result=xmltab:find("ns2:return")
  if result and type(result) == "table" 
  then
    return result[1];
  end
  return nil
end

local function GetSOAPCards(xmlresp)
  local xmltab=XML.eval(xmlresp)
  local result=xmltab:find("ns2:cards")
  return result
end

local function getLookupListResponse(xmlresp)
  local xmltab=XML.eval(xmlresp)
  local result=xmltab:find("ns2:getLookupListResponse")
  if result and type(result) == "table" 
  then
    return result[1];
  end
  return nil
end

local function isin(str,array)
  local theResult=false
  for i,v in ipairs(array)
  do
    if(v == str)
    then
        theResult=true
    end
  end
  return theResult
end

function xml_escape(value)
    return value:gsub("&","&amp;"):gsub([["]],"&quot;"):gsub("'","&apos;"):gsub("<","&lt;"):gsub(">","&gt;");
end

local Dictionaries={}

Dictionaries["List"]={"EquipmentClass","ValueTypes","host_status","LOC_TYPE","snmp_available",
                        "zItemStatus","jmx_available","ipmi_privilege","data_type","ipmi_authtype",
                        "ztriggerState","valuemaps","EquipmentStatus","snmpv3_securitylevel",
                        "snmpv3_authprotocol","ztriggerType","ztriggerValue","units","HostStatus",
                        "flags","maintenance_type","delta","ipmi_available","maintenance_status",
                        "snmpv3_privprotocol","ItemType","zItemState","auth_type","BooleanEnum",
                        "Severity","MacroType"
}

Dictionaries["lookups"]={}
Dictionaries["TColumn2Lookup"]={}

function Dictionaries.load()
    table.foreach(Dictionaries.List,function(key,value)
      local xmltext=soap.getLookupList("admin","admin",value)
      local xmlresp=soap.retriveMessage(soap.Send(WS_URL,xmltext))
      local retval=getLookupListResponse(xmlresp)
      local result=(XML.eval(xmlresp))
      for i=1,table.maxn(result)
      do
        for j=1,table.maxn(result[i])
        do
          for k=1,table.maxn(result[i][j])
          do
            if Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]]
            then
                Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]][tostring(result[i][j][k]:find("ns2:code")[1])]=result[i][j][k]:find("ns2:id")[1]
            else
                Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]]={}
                Dictionaries.lookups[result[i][j][k]:find("ns2:type")[1]][tostring(result[i][j][k]:find("ns2:code")[1])]=result[i][j][k]:find("ns2:id")[1]
            end
          end
        end
      end
    end)

    Dictionaries.TColumn2Lookup["templates"]={}
    Dictionaries.TColumn2Lookup["Hosts"]={}
    Dictionaries.TColumn2Lookup["HostTypes"]={}
    Dictionaries.TColumn2Lookup["ztriggers"]={}
    Dictionaries.TColumn2Lookup["zItems"]={}
    Dictionaries.TColumn2Lookup["zUsermacro"]={}
    Dictionaries.TColumn2Lookup.templates["ipmi_authtype"]="ipmi_authtype"
    Dictionaries.TColumn2Lookup.templates["flags"]="flags"
    Dictionaries.TColumn2Lookup.templates["status"]="host_status"
    Dictionaries.TColumn2Lookup.templates["snmp_available"]="snmp_available"
    Dictionaries.TColumn2Lookup.templates["maintenance_type"]="maintenance_type"
    Dictionaries.TColumn2Lookup.templates["maintenance_status"]="maintenance_status"
    Dictionaries.TColumn2Lookup.templates["jmx_available"]="jmx_available"
    Dictionaries.TColumn2Lookup.templates["ipmi_privilege"]="ipmi_privilege"
    Dictionaries.TColumn2Lookup.templates["ipmi_available"]="ipmi_available"
    Dictionaries.TColumn2Lookup.HostTypes=Dictionaries.TColumn2Lookup.templates;
    Dictionaries.TColumn2Lookup.Hosts=Dictionaries.TColumn2Lookup.templates;
    Dictionaries.TColumn2Lookup.Hosts["OpStatus"]="HostStatus"
    Dictionaries.TColumn2Lookup.ztriggers["type"]="ztriggerType"
    Dictionaries.TColumn2Lookup.ztriggers["flags"]="flags"
    Dictionaries.TColumn2Lookup.ztriggers["state"]="ztriggerState"
    Dictionaries.TColumn2Lookup.ztriggers["status"]="zItemStatus"
    Dictionaries.TColumn2Lookup.ztriggers["value"]="ztriggerValue"
    Dictionaries.TColumn2Lookup.ztriggers["priority"]="Severity"
    Dictionaries.TColumn2Lookup.zItems["type"]="ItemType"
    Dictionaries.TColumn2Lookup.zItems["value_type"]="ValueTypes"
    Dictionaries.TColumn2Lookup.zItems["authtype"]="auth_type"
    Dictionaries.TColumn2Lookup.zItems["data_type"]="data_type"
    Dictionaries.TColumn2Lookup.zItems["delta"]="delta"
    Dictionaries.TColumn2Lookup.zItems["flags"]="flags"
    Dictionaries.TColumn2Lookup.zItems["multiplier"]="BooleanEnum"
    Dictionaries.TColumn2Lookup.zItems["snmpv3_authprotocol"]="snmpv3_authprotocol"
    Dictionaries.TColumn2Lookup.zItems["snmpv3_privprotocol"]="snmpv3_privprotocol"
    Dictionaries.TColumn2Lookup.zItems["snmpv3_securitylevel"]="snmpv3_securitylevel"
    Dictionaries.TColumn2Lookup.zItems["state"]="zItemState"
    Dictionaries.TColumn2Lookup.zItems["status"]="zItemStatus"
    Dictionaries.TColumn2Lookup.zItems["valuemapid"]="valuemaps"
    Dictionaries.TColumn2Lookup.zItems["units"]="units"
    Dictionaries.TColumn2Lookup.zUsermacro["type"]="MacroType"
end

function Dictionaries.getLookUpId(t,c)
    if t and c
    then
        local retval=Dictionaries.lookups[t][tostring(c)]
        if(retval)
        then
            return retval
        else
            error("Dictionary is incomplete for lookup: "..t.." with the code: "..c);
        end
    else
        error("Dictionary is incomplete or nil reference being requested for lookup: "..t.." with the code: "..c);
    end
end



local dict=Dictionaries

local function isempty(s)
  return s == nil or s == ''
end

--[[
	Formatted printing
]]-- 
local function printf(fmt, ...)
	print(string.format(fmt, unpack(arg)))
end
--[[

]]--
function soap2graph()
	-- Convenience
	local node, edge, subgraph, cluster, digraph, strictdigraph =
	graph.node, graph.edge, graph.subgraph, graph.cluster, graph.digraph, graph.strictdigraph

	local g = strictdigraph{"G",
	  compound = "1",
	  rankdir = "LR",
	  size="6.5,6.5",
	  comment = "LuaGraph: testGraph.lua",
	  cluster{"c1",
		edge{
		  node{"n1", comment="123"},
		  node{"n2"},
		  label = "n1-n2"
		},
	  },
	  cluster{"c2",
		edge{
		  node{"m1"},
		  node{"m2"},
		  node{"m3"},
		  label = "m1-m2-m3"
		},
		edge{"m3", "m1"},
	  },
	  node{"o1"},
	  edge{"n1", "m2", ltail="cluster_c1", lhead="cluster_c2", label="comp"},
	  edge{"n1","o1", "m1", label="nom"}
	}

	-- Show the graph using dotty
	if true then
	  local fn = os.tmpname()..".dot"
	  g:write()
	  g:write(fn)
	  os.execute("dotty "..fn)
	  os.remove(fn)
	end

	-- Make the layout using 'dot' (default) engine 
	print("Layout ...")
	g:layout()

	-- Render the graph into postscript format
	print("Render MIMIC...")
	--[[ 
		Other variable
		___________________________
		g:render("ps", "testGraph.ps")
		g:render("gif", "testGraph.gif")
		g:render("png", "testGraph.png")
	]]--
	g:render("svg", "testGraph.svg")

	-- Show the graph
	g:show()
	-- Close the graph 
	print("Close ...")
	g:close()
end

function main()
      print("Loading dictionaries...")
      Dictionaries.load()
      print("finished")
      -- zapi.tracetoggle()
      soap2graph()
end
main()