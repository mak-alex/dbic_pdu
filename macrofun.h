

#define SetBit(x,y) 		(x|=y)
#define ClrBit(x,y) 		(x&=~y)
#define TestBit(x,y) 		(x&y)

#define MAX_DS1820    		8
#define ADC_VREF_TYPE 		0x40
 
#define UDRE 5
#define DATA_REGISTER_EMPTY (1<<UDRE)
 
 
