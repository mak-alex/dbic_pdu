/*****************************************************************************
����: RTL8019.c
Created by PROTTOSS Mail to PROTTOSS@mail.ru

RTL8019AS driver unit

05.06.2007
******************************************************************************/

//#include <ioavr.h>
//#include <intrinsics.h>

//#include "stdafx.h"
//#include "ethernet.h"

#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include <util/delay.h>


#include "RTL8019.h"

/*****************************************************************************
debug messages
******************************************************************************/

//#define DBG_RTL

#ifdef DBG_RTL
#define DBG_OUT(...)	printf(__VA_ARGS__)
#else
#define DBG_OUT(...)
#endif	/* #ifdef DBG_RTL */

/*****************************************************************************
Structures
******************************************************************************/

/* Realtek receive packet header */
typedef struct
{
    UINT8	status;		/* Status, contents of RSR register */
    UINT8	next_page;	/* Page for next packet */
    UINT16	size;   	/* Size of header and packet in octets */

} RTL_Packet_Header_t;
#define RTL_PACKET_HEADER_SIZE sizeof(RTL_Packet_Header_t)

/*****************************************************************************
Setup
******************************************************************************/

/* Size of a single ring buffer page. */
#define RTL_PAGE_SIZE   256

/* First ring buffer page address.*/
#define RTL_START_PAGE  0x40

/* Last ring buffer page address plus 1. */
#define RTL_STOP_PAGE   0x60

/*  Number of pages in a single transmit buffer. This should be at least the MTU size. */
#define RTL_TX_PAGES    6
#define RTL_TX_BUFFERS  1
#define RTL_FIRST_TX_PAGE   RTL_START_PAGE

/* First ring buffer RX page address.*/
#define RTL_FIRST_RX_PAGE   (RTL_FIRST_TX_PAGE + RTL_TX_PAGES * RTL_TX_BUFFERS)

/*****************************************************************************
Definitions
******************************************************************************/

/* in/out commands */
#define RTL_REG(reg)         (*((volatile UINT8 *)RTL_BASE_ADDR + reg))

/* enable/disable interrupts */
#define RTL_ISR_ENABLE()		RTL_INT_MASK_REG |= (1 << RTL_INT_MASK_BIT)
#define RTL_ISR_DISABLE()		RTL_INT_MASK_REG &= ~(1 << RTL_INT_MASK_BIT)

/* Clear interrupts */
#define RTL_CLEAR_INTR()	RTL_REG(ISRRTL) = 0xff


/*****************************************************************************
Variables
******************************************************************************/

/*****************************************************************************
Function prototypes
******************************************************************************/

/* Detect RTL8019AS chip */
BOOL RTL_Detect(void);

/* Setup interrupt service routine */
void RTL_ISRSetup(void);

/* Setup CONFIG registers */
void RTL_CONFIGSetup(void);

/* Chip start */
void RTL_Start(UINT8 *mac);

/* buffers overflow */
UINT8 RTL_Overflow(void);

/*****************************************************************************
Module init
******************************************************************************/
BOOL RTL_Init(UINT8 *mac)
{	
  	DBG_OUT("RTL_Init: Start...\n\r");
  	
  	/* wait for NIC internal initialize */
  	_delay_ms(50);

    /* Detect NIC */
    if(!RTL_Detect())
    {	
      	DBG_OUT("RTL_Init error: chip not detected!\n\r");
      	return FALSE;
    }

    /* Load CONFIG registers */
    RTL_CONFIGSetup();
	
    /* Start the NIC */
    RTL_Start(mac);
	
    DBG_OUT("RTL_Init: Done!\n\r");
    return TRUE;
}

/*****************************************************************************
Setup interrupt line
******************************************************************************/
void RTL_ISRSetup(void)
{	
  	/* setup IO port */
  	RTL_INT_PORT |= (1 << RTL_INT_PIN_BIT);	/* include pull-ups */
    RTL_INT_DDR	&= ~(1 << RTL_INT_PIN_BIT); /* in direction */
  	
  	/* setup rising edge between two samples of INT7 generates an interrupt request */
  	RTL_INT_CTRL_REG |= (1 << RTL_INT_CTRL_REG_B0) | (1 << RTL_INT_CTRL_REG_B1);
}

/*****************************************************************************
Detect RTL8019AS chip
return TRUE if chip ID correctly, else return FALSE
******************************************************************************/
BOOL RTL_Detect(void)
{	
  	DBG_OUT("RTL_Detect: Start...\n\r");
  	
  	/* Stop NIC, DMA abort, include Page0, and exec detect cycle */
  	RTL_REG(CR) = NIC_STOP | DMA_COMPLETE, REG_PAGE0;
  	for (UINT8 j = 0; j < 20; j++)
    {
        UINT8 a = RTL_REG(RESET);	/* Soft reset NIC */
        _delay_ms(5);
        RTL_REG(RESET) = a;
        for (UINT8 i = 0; i < 20; i++)
        {
            _delay_ms(50);

            /* Get RTL8019AS ID world */
            if ((RTL_REG(ISRRTL) & ISR_RST) != 0 &&     /* chip reset? */
                RTL_REG(RBCR0) == 0x50 &&	/* first ID byte */
                RTL_REG(RBCR1) == 0x70)		/* second ID byte */
            {
              	
              	DBG_OUT("RTL_Detect: RTL8019AS chip detected! Done.\n\r");
                return TRUE;	/* succes! */
            }
        }

        DBG_OUT("RTL_Detect warning: Not chip detected. RBCR0 = 0x%X; RBCR1 = 0x%X... Try again...\n\r", RBCR0, RBCR1);
    }
	
    /* Error! Chip not detected! */
    DBG_OUT("RTL_Detect error: Not chip detected.\n\r");
    return FALSE;
}

/*****************************************************************************
Load CONFIG registers
******************************************************************************/
void RTL_CONFIGSetup(void)
{
  	/* switch to page 3 to load config registers */
    RTL_REG(CR) = NIC_STOP | DMA_COMPLETE |  REG_PAGE3;

    /* EEPROM write enable of config registers */
    RTL_REG(EEPROMCR) = EECR_EEM1 | EECR_EEM0;

    /* enable IRQ line */
    RTL_REG(CONFIG1) = CONFIG1_IRQEN;

    /* set network type to 10BaseT with link test disabled */
    RTL_REG(CONFIG2) = CONFIG2_BSELB;

    /* disable powerdown and sleep modes */
    RTL_REG(CONFIG3) = 0;

    /* Reenable write protection of the RTL configuration registers
    and wait for link test to complete. */
    _delay_ms(255);
    RTL_REG(EEPROMCR) = 0;
}

/*****************************************************************************
NIC start
******************************************************************************/
void RTL_Start(UINT8 *mac)
{	
  	/* Switch to register page 0 and set data configuration register
    to byte-wide DMA transfers, normal operation (no loopback),
    send command not executed and 8 byte fifo threshold */
    RTL_REG(CR) = NIC_STOP | DMA_COMPLETE | REG_PAGE0;
    _delay_ms(5);	/* Make sure nothing is coming in or going out */
    RTL_REG(DCR) = DCR_LS | DCR_FT1 | DCR_AR;

    /* Mask all interrupts, clear any interrupt status flag, and setup interrupt line */
    RTL_REG(IMR) = 0;
    RTL_CLEAR_INTR();
    RTL_ISRSetup();

    /* Clear remote dma byte count register. */
    RTL_REG(RBCR0) = 0;
    RTL_REG(RBCR1) = 0;

    /* Temporarily set receiver to monitor mode and transmitter to
     * internal loopback mode. Incoming packets will not be stored
     * in the RTL ring buffer and no data will be send to the network. */
    RTL_REG(RCR) = RCR_MON;
    RTL_REG(TCR) = TCR_LB0;

    /* Configure the RTL's ring buffer page layout. */
    RTL_REG(TPSR) = RTL_FIRST_TX_PAGE;
    RTL_REG(BNRY) = RTL_STOP_PAGE - 1;		/* Last page read. */
    RTL_REG(PSTART) = RTL_FIRST_RX_PAGE;	/* First page of receiver buffer. */
    RTL_REG(PSTOP) = RTL_STOP_PAGE;			/* Last page of receiver buffer. */

    /* Switch to register page 1 and copy our MAC address into the RTL */
    RTL_REG(CR) = NIC_STOP | DMA_COMPLETE | REG_PAGE1;
    for (UINT8 i = 0; i < 6; i++)
        RTL_REG(PAR0 + i) = *mac++;

    /* Clear multicast filter bits to disable all packets. */
    for (UINT8 i = 0; i < 8; i++)
        RTL_REG(MAR0 + i) = 0xff;

    /* Set current page pointer to one page after the boundary pointer. */
    RTL_REG(CURR) = RTL_FIRST_RX_PAGE;

    /* Switch back to register page 0, remaining in stop mode. */
    RTL_REG(CR) = NIC_STOP | DMA_COMPLETE | REG_PAGE0;
    _delay_ms(5);	/* Make sure nothing is coming in or going out */

    /* Take receiver out of monitor mode and enable it for accepting broadcasts. */
    RTL_REG(RCR) = RCR_AB;

    /* Clear all interrupt status flags and enable interrupts
    Note: transmitter if polled, thus no IMR_PTXE */
    RTL_CLEAR_INTR();
    RTL_REG(IMR) = RTL_INT_FLAGS_SETUP;

    /* clear interrupts flags */
    RTL_REG(ISRRTL) = 0xff;

    /* enable interrupts */
    RTL_ISR_ENABLE();

    /* Fire up the RTL by clearing the stop bit and setting the start bit.
    To activate the local receive dma we must also take the RTL out of
    the local loopback mode. */
    RTL_REG(TCR) = 0;
    RTL_REG(CR) = NIC_START | DMA_COMPLETE | REG_PAGE0;

    /* */
    _delay_ms(255);
}

/*****************************************************************************
Read data with NIC
******************************************************************************/
void RTL_DMARead(UINT8 *buf, UINT16 len)
{	
  	while(len--)
    	*buf++ = RTL_REG(IOPORT);
}

/*****************************************************************************
Write data to NIC
******************************************************************************/
/*
void RTL_DMAWrite(UINT8 *nbuf)
{	
  	while(NULL != nbuf)
    {	
      	UINT8 *data = nbuf->data;
      	while(0 < nbuf->data_len--)
        	RTL_REG(IOPORT) = *data++;
     	
        nbuf = nbuf->next;
    }
}
*/
/*****************************************************************************
Prepare DMA byte count ahd start address
******************************************************************************/
void RTL_DMAPrepare(UINT8 cmd, UINT16 addr, UINT16 len)
{	
  	/* Set remote DMA byte count and start address. */
    RTL_REG(RBCR0) = LOBYTE(len);
    RTL_REG(RBCR1) = HIBYTE(len);
    RTL_REG(RSAR0) = LOBYTE(addr);
    RTL_REG(RSAR1) = HIBYTE(addr);

    /* Set NIC command */
    RTL_REG(CR) = cmd;
}

/*****************************************************************************
Complete remote DMA
******************************************************************************/
void RTL_DMAComplete(void)
{
    /* Abort DMA */
    RTL_REG(CR) = NIC_START | DMA_COMPLETE | REG_PAGE0;
#if 1
    /* Check that we have a DMA complete flag.*/
    for(UINT8 i = 0; i <= 20; i++)
        if(RTL_REG(ISRRTL) & ISR_RDC)
            break;
#endif
    /* Reset remote DMA complete flag. */
    RTL_REG(ISRRTL) = ISR_RDC;
}



/*****************************************************************************
Fetch the next packet out of the receive ring buffer
return packet lenght if receive packet complete, else -1

First word in buf = RX packet lenght
******************************************************************************/
UINT16 RTL_GetPacket(UINT8 *buf)
{	
    /* we don't want to be interrupted by RTL owerflow */
    RTL_ISR_DISABLE();

    /* Get the current page pointer. It points to the page where the NIC
    will start saving the next incoming packet */
    RTL_REG(CR) = NIC_START | DMA_COMPLETE | REG_PAGE1;
    UINT8 curr = RTL_REG(CURR);
    RTL_REG(CR) = NIC_START | DMA_COMPLETE | REG_PAGE0;

    /* Get the pointer to the last page we read from. The following page is the one where we
    start reading. If it's equal to the current page pointer, then there's nothing to read.
    In this case we return a FALSE */
    UINT8 bnry;
    if((bnry = RTL_REG(BNRY) + 1) >= RTL_STOP_PAGE)
        bnry = RTL_FIRST_RX_PAGE;
	
    if(bnry == curr)
    {	
      	RTL_ISR_ENABLE();
        return 0; // MAXUINT16;
    }

    /* Read the NIC specific packet header */
    RTL_Packet_Header_t hdr;
    RTL_DMAPrepare(NIC_START_DMA_READ, MAKEUINT16(bnry, 0), RTL_PACKET_HEADER_SIZE);
    RTL_DMARead((UINT8 *)&hdr, RTL_PACKET_HEADER_SIZE);
    RTL_DMAComplete();

    /* Calculate the page of the next packet. If it differs from the
    pointer in the packet header, we return with error code */
    UINT8 nextpg = bnry + HIBYTE(hdr.size) + (LOBYTE(hdr.size) != 0);
    if(nextpg >= RTL_STOP_PAGE)
    {
        nextpg -= RTL_STOP_PAGE;
        nextpg += RTL_FIRST_RX_PAGE;
    }
    if(nextpg != hdr.next_page)
    {
        UINT8 nextpg1 = nextpg + 1;
        if(nextpg1 >= RTL_STOP_PAGE)
        {
            nextpg1 -= RTL_STOP_PAGE;
            nextpg1 += RTL_FIRST_RX_PAGE;
        }
        if (nextpg1 != hdr.next_page)
        {	
          	RTL_ISR_ENABLE();
            return 0; //MAXUINT16;
        }
        nextpg = nextpg1;
    }

    /* Read the data. Don't read the header again  */
    UINT16 count = hdr.size - RTL_PACKET_HEADER_SIZE;
    DBG_OUT("RTL_GetPacket: packet %d bytes received!\n\r", count);

    /* Check packet length and status. Silently discard packets of illegal size */
    if((count >= ETH_MIN_PACKET_SIZE)	&&
       (count <= ETH_MAX_PACKET_SIZE)	&&
       (!(hdr.status & (RSR_CRC | RSR_FAE | RSR_FO))))
    {  	
      	if(buf)	/* check valid buffer pointer and read packet data */
        {	
          	RTL_DMAPrepare(NIC_START_DMA_READ, MAKEUINT16(bnry, RTL_PACKET_HEADER_SIZE), count);
           	RTL_DMARead(buf, count);
           	RTL_DMAComplete();
        }
    }
    else	/* invalid packet */
    	count = 0; //MAXUINT16;

    /* Set boundary register to the last page we read. This also drops packets with errors */
    if (--nextpg < RTL_FIRST_RX_PAGE)
        nextpg = RTL_STOP_PAGE - 1;
    RTL_REG(BNRY) = nextpg;
	
    /* enable NIC interrupt and return OK*/
    RTL_ISR_ENABLE();
    return count;
}

/*****************************************************************************
Fetch the next packet out of the receive ring buffer

Input:
Adress of NET_Buffer_t structure

Return:
if success, return TRUE, else return FALSE
******************************************************************************/
//BOOL RTL_PutPacket(NET_Buffer_t *nbuf)
BOOL RTL_PutPacket(UINT16 len, UINT8* packet)
{	
  	DBG_OUT("RTL_PutPacket: Start...\n\r");
	
    /* check lenght */
  	if(len > ETH_MAX_PACKET_SIZE)
    {
      	DBG_OUT("RTL_PutPacket error: packet lenght too long - %d bytes\n\r", len);
    	return FALSE;
    }
    UINT16 pl;
    /* The controller will not append pad bytes, so we have to do this */
    UINT16 padding = 0;
    if(len < ETH_MIN_PACKET_SIZE)
    	padding = ETH_MIN_PACKET_SIZE - len;

    /* still transmitting a packet - wait for it to finish */
    while(RTL_REG(CR) & CR_TXP){};

    /* we don't want to be interrupted by NIC owerflow */
    RTL_ISR_DISABLE();

    /* Set remote dma byte count and start address */
    RTL_DMAPrepare(NIC_START_DMA_WRITE, MAKEUINT16(RTL_FIRST_TX_PAGE, 0), len + padding);
	//RTL_DMAWrite(packet);
    //Write data to NIC
    for (UINT16 i = 0; i < len; i++)
        RTL_REG(IOPORT) = packet[i];


    /* Add pad bytes. */
#if 0
    for (UINT16 i = 0; i < padding; i++)
        RTL_REG(IOPORT) = 0;
#endif

    /* Complete remote DMA */
    RTL_DMAComplete();

    /* Number of bytes to be transmitted. */
	pl = len + padding;
    RTL_REG(TBCR0) = LOBYTE(pl);
    RTL_REG(TBCR1) = HIBYTE(pl);

    /* First page of packet to be transmitted */
    RTL_REG(TPSR) =  RTL_FIRST_TX_PAGE;

    /* Start transmission */
    RTL_REG(CR) = CR_TXP | DMA_COMPLETE;

    /* enable NIC interrupt and return OK*/
    RTL_ISR_ENABLE();

    DBG_OUT("RTL_PutPacket: Done! Sending %d bytes\n\r", len + padding);
    return TRUE;
}

/*****************************************************************************
RTL buffers overflow
******************************************************************************/
UINT8 RTL_Overflow(void)
{
    UINT8 cr, curr, resend = 0;

    /* Wait for any transmission in progress. Save the command register,
    so we can later determine, if RTL transmitter has been interrupted.
    or reception in progress. */
    while(RTL_REG(CR) & CR_TXP);
    cr = RTL_REG(CR);

    /*
     * Get the current page pointer. It points to the page where the RTL
     * will start saving the next incoming packet.
     */
    RTL_REG(CR) = CR_STP | CR_RD2 | CR_PS0;
    curr = RTL_REG(CURR);
    RTL_REG(CR) = CR_STP | CR_RD2;

    /* Clear remote byte count register. */
    RTL_REG(RBCR0) = 0;
    RTL_REG(RBCR1) = 0;

    /* Check for any incomplete transmission. */
    if((cr & CR_TXP) && ((RTL_REG(ISRRTL) & (ISR_PTX | ISR_TXE)) == 0))
        resend = 1;

    /* Enter loopback mode and restart the RTL. */
    RTL_REG(TCR) = TCR_LB0;
    RTL_REG(CR) = NIC_START | DMA_COMPLETE | REG_PAGE0;

    /* Discard all packets from the receiver buffer. Set boundary register to the last page we read */
    if(--curr < RTL_FIRST_RX_PAGE)
        curr = RTL_STOP_PAGE - 1;
    RTL_REG(BNRY) = curr;

    /* Switch from loopback to normal mode mode. */
    RTL_REG(TCR) = 0;

    /* Re-invoke any interrupted transmission. */
    if(resend)
        RTL_REG(CR) = (NIC_START | CR_TXP | DMA_COMPLETE);

    /* Finally clear the overflow flag */
    RTL_REG(ISRRTL) = ISR_OVW;
    return resend;
}

/*****************************************************************************
RTL8019 interrupt service routine
******************************************************************************/
//#pragma vector = RTL_INT_VECTOR
//__interrupt void RTL_ISR(void)


ISR (RTL_INT_VECTOR)

{	
  	/* store interrupt flag and clear it */
  	UINT8 isrf = RTL_REG(ISRRTL);	
    RTL_REG(ISRRTL) = isrf;

    /* check NIC Overflow */
    if(isrf & ISR_OVW)
       	RTL_Overflow();  	
}

